var circleCircum = (radius) => {
    const pi = 3.1416
    return pi*radius*2

}

var circleArea = (radius) => {
    const pi = 3.1416
    return pi*radius**2
}

let inputRadius
let outputCircum
let outputArea 

inputRadius = Number(prompt("Calculate the circumference and area of a circle.\nEnter a radius (cm):"))

outputCircum = circleCircum(inputRadius);
outputArea = circleArea(inputRadius);

console.log(`Circle Radius (cm)           :          ${inputRadius.toFixed(2)}`)
console.log(`Circle Circumference (cm)    :          ${outputCircum.toFixed(2)}`)
console.log(`Circle Area (cm)             :          ${outputArea.toFixed(2)}`)