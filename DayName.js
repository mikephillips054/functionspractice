let dayName = (dayNum) => {
    const dayArray = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
    return  dayArray[dayNum - 1]
  }

let inputDayNum
let outputDay

inputDayNum = Number(prompt("Determine the name of the day of the week from the day number.\nEnter the day number (1 to 7):"))

outputDay = dayName(inputDayNum)

console.log(`Day Number    : ${(inputdayNum)}`)
console.log(`Day Name      : ${(outputDay)}`) 