//function milesToKm(miles) {
//    return  miles / 0.62137
//}
var milesToKm = (miles) => miles/0.62137  
let inputMiles              
let myKm      
  

inputMiles = Number(prompt("Convert a distance in miles to kilometres.\nEnter the distance in miles:"))
myKm = milesToKm(inputMiles)

console.log(`Distance in Miles      : ${inputMiles.toFixed(2)}`)

console.log(`Distance in Kilometres : ${myKm.toFixed(2)}`)

document.getElementById("output").innerHTML = `Distance in miles${inputMiles}   is: ` + milesToKm(myKm);