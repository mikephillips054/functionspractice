let calcBMI = (weight, height) => {
    return weight/(height/100)**2
}                                                                        //Declaring functions

 let inputWeight                                                        //Declaring variables
 let inputHeight 
 let BMI

inputWeight = Number(prompt("Body Mass Index (BMI) Calculator.\nEnter the person's weight (kg):"))      
inputHeight = Number(prompt("Body Mass Index (BMI) Calculator.\nEnter the person's height (cm):"))          
BMI = calcBMI(inputWeight, inputHeight)                                 //Display outputs for console

console.log("")
console.log("Body Mass Index Calculator")
console.log("--------------------------")
console.log("")

console.log(`Person's weight : ${inputWeight} kg`)                       //Display person's weight
console.log(`Person's height : ${inputHeight} cm`)                       //Display person's height
console.log("------------------------")
console.log(`Body Mass Index : ${BMI.toFixed(2)}`)                       //Display person's BMI