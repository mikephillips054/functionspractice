/*
Title: Miles To Kilometres

We will use the "declaration" method of defining a function. (ES5)

Run this program using an html web page.
*/


//--------------------------------------------------------------------------------------------
//This function accepts a value, ie. an argument is passed to the function (distance in miles)
//--------------------------------------------------------------------------------------------

//Declare function: 
//Function to convert a distance miles to kilometres
function milesToKm(miles) {
  return  miles / 0.62137
}


//Declare variables:
let inputMiles              //User input - distance in miles
let myKm                    //Distance in miles converted to kilometres        

//Get input distance in miles from the user
inputMiles = Number(prompt("Convert a distance in miles to kilometres.\nEnter the distance in miles:"))

//Determine the distance in Kilometres
myKm = milesToKm(inputMiles)

//Display input distance in miles to 2dp
console.log(`Distance in Miles      : ${inputMiles.toFixed(2)}`)

//Display converted distance in kilometres to 2dp
console.log(`Distance in Kilometres : ${myKm.toFixed(2)}`)

document.getElementById("output").innerHTML = `Distance in miles${inputMiles}   is: ` + milesToKm(myKm);

/*
NOTE - The use of variable "myKm" is not necessary.
In this case the calculation and display of the converted distance 
could have been done without the use of intermeditary variables as shown below:

console.log(`Distance in Kilometres : ${milesToKm(inputMiles).toFixed(2)}`)

*/
