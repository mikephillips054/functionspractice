/*
Title:   Calculate BMI
Author:  Stefan
Date:    March 2018
Purpose: Given a person's weight (kg) and height (cm), calculate the person's Body Mass Index (BMI).
         NOTE: A function is used to calculate the BMI
*/

//Declare Functions

//Function Name: calcBMI
//Parameters:    Person's weight (in kg) and height (in cm)
//Purpose:       Calculates and returns the Body Mass Index (BMI)
let calcBMI = (weight, height) => {
    return weight/(height/100)**2          //height in cm, needs to be divided by 100 to convert to metres
}



//Declare variables
let inputWeight                     //Weight of person in kg (user input)
let inputHeight                     //Height of person in cm (user input)  NOTE - input is in CENTIMETRES
let bodyMassIndex                   //Calculated Body Mass Index (output)

//Prompt for and get input from user
                                                            //Person's height (cm)


//Use the "calcBMI" function to determine the BMI
bodyMassIndex = calcBMI(inputWeight, inputHeight) 


//Display heading
console.log("")
console.log("Body Mass Index Calculator")
console.log("--------------------------")
console.log("")


//Display the input weight and height and the BMI results 
console.log(`Person's weight : ${inputWeight} kg`)                       //Display person's weight
console.log(`Person's height : ${inputHeight} cm`)                       //Display person's height
console.log("------------------------")
console.log(`Body Mass Index : ${bodyMassIndex.toFixed(1)}`)             //Display Body Mass Index to one decimal place
